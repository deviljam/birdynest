from django.conf         				import settings
from django.http						import HttpResponse, HttpResponseRedirect

from base64 							import decodestring

from django.contrib.auth 				import authenticate, login, logout
from django.contrib.auth.decorators 	import login_required, user_passes_test
from django.utils 						import simplejson as json
from decorators    						import template_view, json_view
from models								import Article, User, Config

import urllib2
import re
import os
import datetime
import md5
#import cronjobs

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/index.html')
def index( request, context ):
	context['page'] = "home"
	return context

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/articles_index.html')
def articles_index( request, context ):
	types = Config.get("article_types").split(",")
	a_list = Article.objects.filter(parent_id=None).order_by('-created');
	context['articles'] = []
	for a in a_list:
		_a = a.todict()
		context['articles'].append( _a )
	
	context['types'] = types
	context['page'] = "articles"
	return context

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/articles_create.html')
def articles_create( request, context ):
	fields = dict( json.loads( Config.get("custom_fields") ) )
	
	if request.POST:
		try:
			temp = Article()
			temp.title = request.POST['title']
			temp.content = request.POST['content']
			temp.private = 'private' in request.POST
			temp.type = int(request.POST['type'])
			
			if int(request.POST['parent']) != 0:
				temp.parent = Article.objects.get(id=request.POST['parent'])
			else:
				temp.parent = None
				
			#File upload
			for key in request.FILES.keys():
				request.POST[key] = upload_through_file(request.FILES[key])
			
			#Find and add custom data fields
			json_fields = {}
			for key, field in fields.iteritems():
				if key in request.POST:
					json_fields[key] = request.POST[key]
			temp.data = json.dumps( json_fields )			
			
			temp.user = request.user
			temp.save()
			context['redirect'] = "/admin/articles"
		except Exception, err:
			context['error_flash'] = "Unable to create article, please try again: " + str(err) 
	else:
		add_blanks_to_fields(fields);
		context['articles'] = Article.objects.all()
		
	context['sizes'] = dict( json.loads( Config.get("image_sizes") ) )
	context['fields'] = fields
	context['types'] = Config.get("article_types").split(",")
	context['page'] = "articles"
	context['image_old_upload'] = Config.get("image_old_upload").lower() == "true"
	return context
	
@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/articles_edit.html')
def articles_edit( request, context, id ):
	fields = dict( json.loads( Config.get("custom_fields") ) )
	
	if request.POST:
		try:
			temp = Article.objects.get(id=id)
			temp.title = request.POST['title']
			temp.content = request.POST['content']
			temp.private = 'private' in request.POST
			temp.type = int(request.POST['type'])

			if int(request.POST['parent']) != 0:
				temp.parent = Article.objects.get(id=request.POST['parent'])
			else:
				temp.parent = None
				
			#File upload
			for key in request.FILES.keys():
				request.POST[key] = upload_through_file(request.FILES[key])
				
			#Find and add custom data fields
			current_data = json.loads( temp.data )
			json_fields = {}
			for key, field in fields.iteritems():
				if key in request.POST and request.POST[key] != "":
					json_fields[key] = request.POST[key]
				elif key in current_data:
					json_fields[key] = current_data[key]
			temp.data = json.dumps( json_fields )
			
			temp.save()
			context['redirect'] = "/admin/articles"
		except Exception, err:
			context['error_flash'] = "Unable to save article, please try again: " + str(err)
	else:
		article =  Article.objects.get(id=id).todict()
		fields = add_values_to_fields(article["data"], fields)
		context['article'] = article
		context['articles'] = Article.objects.all()
		
		
	context['fields'] = fields
	context['sizes'] = dict( json.loads( Config.get("image_sizes") ) )
	context['types'] = Config.get("article_types").split(",")
	context['page'] = "articles"
	context['image_old_upload'] = Config.get("image_old_upload").lower() == "true"
		
	context['page'] = "articles"
	return context
	
@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/articles_edit.html')
def articles_delete( request, context, id ):
	try:
		Article.objects.get(id=id).delete();
	except Exception, err:
		context['error_flash'] = "Unable to delete article, please try again" + str(err)
			
	context['redirect'] = "/admin/articles"
	
	return context
	
@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/image_manager.html')
def image_manager( request, context ):
	context['images'] = _getListOfImages()
	return context

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@json_view
def upload_image( request ):
	out = {}
	
	if request.POST:
		try:
			path = "app/media/img/uploads/"
			filename = request.POST['name']
			ext = re.match('.*\.(jpg|jpeg|png|bmp|gif)$', filename.lower()).group(1)
			newname = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%M") +"_"+ md5.new(filename).hexdigest()[0:10] +"."+ext
			content = request.POST['content'].split(',')[1]
			
			file = open(str(path+newname), 'wb')
			file.write( decodestring( content ) )
			file.close()
			
			out["new_image"] = newname
					
		except Exception, err:
			pass
			#return HttpResponse('Error' + str( err ) )
	out["images"] = _getListOfImages()
	
	return out
	
def upload_through_file( file ):
	try:
		path = "app/media/img/uploads/"
		srcpath = "/img/uploads/"
		filename = str( file.name )
		ext = re.match('.*\.(jpg|jpeg|png|bmp|gif)$', filename.lower()).group(1)
		newname = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%M") +"_"+ md5.new(filename).hexdigest()[0:10] +"."+ext
		
		f = open(str(path+newname), 'wb')
		for chunk in file.chunks():
			f.write( chunk )
		f.close()
		return srcpath+newname
	except Exception, err:
		return ""
	
@template_view('admin/login.html')
def user_login( request, context, section ):
	#import pdb; pdb.set_trace()
	
	if request.POST:
		try:
			u = request.POST['email']
			try:
				u.index("@")
				u = User.objects.get(email=u).username
			except ValueError:
				pass
			p = request.POST['password']
			user = authenticate(username=u,password=p)
			next = "/admin"
			if next in request.GET:
				next = request.GET['next']
				context['redirect'] = next
			if user is not None:
				login(request, user)
				context['redirect'] = "/admin/"
		except:
			pass
	else:
		pass
		
	if 'redirect' not in context:
		context['user'] = request.user
		
	return context
	
def user_logout(request):	
	logout(request)
	
	res = HttpResponseRedirect("/login")	
	return res;

@template_view('admin/users_index.html')
def users_index(request,context):
	context['page'] = "users"
	context['users'] = User.objects.all()
	return context
	
@template_view('admin/users_edit.html')
def users_edit(request,context,id):
	context['page'] = "users"
	
	u = User.objects.get(id=id)
	if request.POST:
		try:
			username = request.POST['username']
			email = request.POST['email']
			status = int( request.POST['status'] )
			p1 = request.POST['password']
			p2 = request.POST['password2']
			
			#if username != u.username and u.is_superuser :
			#	raise Exception( "You cannot change the details of a super user." )
			if username != u.username and User.objects.filter(username=username).count():
				raise Exception( "A user with this name already exists." )
			elif email != u.email and User.objects.filter(email=email).count():
				raise Exception( "A user with this email already exists." )
			
				
			u.username = username
			u.email = email
			
			if len( p1 ) > 0:
				if p1 != p2:
					raise Exception( "New password does not match" )
				elif len( p1 ) < 6:
					raise Exception( "A password must be at least 6 characters long" )
				else:
					u.set_password(p1)
			
			#Set user status
			u.is_active = status > 0
			u.is_staff = status > 1
			u.is_superuser = status > 2
			
			u.save()
			
			context['redirect'] = "/admin/users/"
		except Exception, err:
			context['u'] = {"id":id,"username":username,"email":email}
			context['error_flash'] = err.message
	else:
		context['u'] = u
	return context

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/config_index.html')
def config_index( request, context ):
	context['configs'] = Config.objects.all()
	context['page'] = "config"
	return context

@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/config_create.html')
def config_create( request, context ):
	
	if request.POST:
		try:
			#import pdb; pdb.set_trace()
			temp = Config()
			temp.name = request.POST['name']
			temp.value = request.POST['value']
			temp.save()
			context['redirect'] = "/admin/config"
		except Exception, err:
			context['error_flash'] = "Unable to save configuration, please try again" + str(err)
		
	context['page'] = "config"
	return context
	
@user_passes_test(lambda u:u.is_superuser, login_url='/login/')
@template_view('admin/config_edit.html')
def config_edit( request, context, id ):
	
	if request.POST:
		try:
			#import pdb; pdb.set_trace()			
			temp = Config.objects.get(id=id)
			temp.name = request.POST['name']
			temp.value = request.POST['value']
			temp.save()
			context['redirect'] = "/admin/config"
		except Exception, err:
			context['error_flash'] = "Unable to save configuration, please try again" + str(err)
	else:
		context['config'] = Config.objects.get(id=id)
		
	context['page'] = "config"
	return context

#USEFUL FUNCTIONS
def _getListOfImages():
	out = []
	#import pdb; pdb.set_trace()
	image_path = 'media'+ os.sep +'img'+ os.sep +'uploads'
	root_path = re.sub( "blog$", "", os.path.dirname(os.path.realpath(__file__)) )	
	full_path = root_path + image_path
	imgs = os.listdir( full_path )
	
	for img in imgs:
		ext = str.lower( img[len(img)-4:len(img)] )
		if ext == '.bmp' or ext == '.jpg' or ext == '.png' or ext == '.gif':
			out.append( img )
		
	return out
	
def image_request( request, image_name ):
	#import pdb; pdb.set_trace()
	from PIL import Image
	
	image_path = 'media'+ os.sep +'img'+ os.sep +'uploads'
	root_path = re.sub( "blog$", "", os.path.dirname(os.path.realpath(__file__)) )	
	full_path = root_path + image_path + os.sep
	
	img_path = full_path + image_name
	res_path = img_path
	sizes = json.loads( Config.get("image_sizes") )
	
	width = 450
	size = 'norm'
	if 'size' in request.GET :
		t_size = request.GET['size']
		if t_size in sizes:
			width = sizes[ t_size ]
			size = t_size
			
	cache_name = size + "_" + image_name
	cache_path = full_path + 'cache' + os.sep + cache_name
	if width > 0:
		if not os.path.exists( cache_path ):
			try:
				#import pdb; pdb.set_trace()
				img = Image.open(img_path)
				org_width, org_height = img.size
				if width < 0:
					width = org_width
				factor = float( width ) / org_width
				out = img.resize(
					(width,int(factor*org_height)),
					Image.BILINEAR)
				out.save(cache_path,'jpeg',quality=90)
				res_path = cache_path
			except Exception, err:
				return HttpResponse( "Error : " + str(err) )
		else:
			res_path = cache_path
	
	res = open( res_path, "r" )
	return HttpResponse(res.read(), mimetype="image/jpeg")
	
def add_values_to_fields(data,fields):
	for key, field in data.iteritems():
		if key in fields :
			fields[key]["value"] = field
	return fields
	
def add_blanks_to_fields(fields):
	for key, field in fields.iteritems():
		fields[key]["value"] = ""
	return fields
	