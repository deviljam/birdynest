from django.conf         				import settings
from django.http						import HttpResponse, HttpResponseRedirect

from django.db.models 					import Q
from django.contrib.auth 				import authenticate, login, logout
from decorators    						import json_view
from models								import Article, User, Comment

import re

#Articles
@json_view
def articles( request ):
	context = {"articles":[]}
	_a = Article.objects.filter(private=False).order_by('-created')
	for a in _a:
		context['articles'].append( a.todict() )
		
	return context
	
@json_view
def article( request, id ):
	context = {"success":False}
	
	try:
		article = Article.objects.get(id=id)
		context['article'] = article.todict()
		context['success'] = True			
	except Exception, err:
		context['message'] = err.message
		
	return context
	
#Comments
@json_view
def comment_list( request, art_id ):
	return article( request, art_id )

@json_view
def comment_add( request, art_id ):
	context = {"success":False}
	
	if request.POST:		
		try:
			user = request.user
			article = Article.objects.get(id=art_id)
			content = request.POST['content']
			
			#Some validation
			if user == None:
				raise Exception( "You must be logged in to comment" )
			if article == None:
				raise Exception( "No article was found with an id of: " + str( art_id ) )
			if len( content ) < 1:
				raise Exception( "You cannot post a comment without a body" )
				
			c = Comment()
			c.user = user
			c.article = article
			c.content = content
			c.save()
			context['success'] = True
			context['comment'] = c.todict()
			
		except Exception, err:
			context['message'] = err.message
	else:
		context['message'] = "Add must be a POST request"
	return context
	
#Gallery
@json_view
def galleries( request ):
	context = {}
	context['articles'] = Article.objects.filter(Q(type=2)|Q(type=3),parent_id=None).order_by('-created')
	for a in context['articles']:
		a.sub_articles = a.children.all()[0:24]		
	return context
	
#Store
@json_view
def store( request ):
	context = {}
	context['articles'] = Article.objects.filter(type=4,parent_id=None).order_by('-created')
	for a in context['articles']:
		a.sub_articles = a.children.all()[0:24]		
	return context
	
#User
@json_view
def user_login ( request ):
	#JSON login
	context = {}
	context["success"] = False
	context["message"] = "No post message"
	
	if request.POST:
		u = request.POST['username']
		p = request.POST['password']
		user = authenticate(username=u,password=p)
		if user is not None:
			if user.is_active:
				login(request, user)
				context["success"] = True
				context["message"] = "Welcome"
				context["username"] = user.username
				context["id"] = user.id
			else:
				context["message"] = "This account has not been activated."
		else:
			context["message"] = "Invalid username or password"
		
	return context
	
@json_view
def user_register ( request ):
	#JSON register
	context = {}
	context["success"] = False
	context["message"] = "No post message"
	
	if request.POST:
		#import pdb; pdb.set_trace()
		try:
			u = request.POST['username'].lower()
			p1 = request.POST['password']
			p2 = request.POST['password2']
			e = request.POST['email'].lower()
			try:
				if p1 != p2:
					raise Exception( "The passwords do not match" )
				if len(p1) < 6:
					raise Exception( "The password is too short, it must be at least 6 characters." )
				if re.match("^[^\s][a-zA-Z0-9_]+[^\s]$", u) == None :
					raise Exception( "Invalid username, a username cannot use spaces or special characters." )
				if re.match("^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$", e) == None :
					raise Exception( "Invalid email address." )
				if User.objects.filter(username=u).count():
					raise Exception( "We're sorry, this username has already been taken." )
				if User.objects.filter(email=e).count():
					raise Exception( "An account has already been created with this email. Please contact the admin to recover an old account." )
				user = User()
				user.username = u
				user.set_password( p1 )
				user.email = e
				user.is_staff = False
				user.is_active = False
				user.save()
				context["success"] = True
				context["message"] = "An account has been created. An email has been sent to your account"
				context["username"] = user.username
			except Exception, err:
				context["message"] = err.message
		except KeyError, err:
			context["message"] = "One or more required fields missing"
		
	return context

@json_view
def user_logout ( request ):
	context = {}
	context["success"] = False
	context["message"] = "Unable to logout, please try again."
	
	try:
		logout( request )
		context["success"] = True
		context["message"] = "You are now logged out."
	except Exception, err:
		pass
	return context