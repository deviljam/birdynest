import re

def format(str, level=0):
	#level 0, basic: links and paragraphs
	#level 1, medium: formatting
	#level 2, complete: The works, images, youtube videos, etc
	
	str = str.replace( "<", "&lt;" )
	str = str.replace( ">", "&gt;" )
	
	str = re.sub("(\s|\n|\r|^)(https?://[^\s<>]+)(\s|\n|\r|$)", lambda m: " <a href='%s' target='_blank'>%s</a> " % (m.group(2),m.group(2)), str)
	
	if level >= 1:
		str = re.sub("\!\[([^\]]+)\]\(([^()]+)\)+", lambda m: "<img alt='%s' src='%s' />" % (m.group(1),m.group(2)), str)
		str = re.sub("\[([^\]]+)\]\(([^()]+)\)+", lambda m: "<a href='%s' target='_blank'>%s</a>" % (m.group(2),m.group(1)), str)
	
	if level >= 2:
		str = re.sub("\'{3}([^<>]+?)\'{3}", lambda m: "<strong>%s</strong>" % m.group(1), str)
		str = re.sub("\'{2}([^<>]+?)\'{2}", lambda m: "<i>%s</i>" % m.group(1), str)
		str = re.sub("youtube\(([^,]+)(,[^,])*\)","<iframe frameborder='0'  width='450' height='253' src='http://www.youtube.com/embed/\g<1>' allowfullscreen></iframe>",str)
		
	str = re.sub("\|(.*)\|", lambda m: "%s" % table(m.group(1)),str,flags=re.DOTALL)

	str = "<p>" + str + "</p>"
	str = re.sub(r"\n|\r", "</p><p>", str )
	
	#str = re.sub("\[(.*)\]", lambda m: "%s" % table(m.group(1)),str,flags=re.MULTILINE)
	
	return str

def table(str):
	str = re.sub("\|(\n|\r)", "</td></tr><tr><td>",str)
	str = re.sub("(\n|\r)\|", "",str)
	str = re.sub("\|", "</td><td>",str)
	out = "<table><tr><td>"+str+"</td></tr></table>"
	return out