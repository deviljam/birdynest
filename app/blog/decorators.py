# stdlib imports
import logging
import urllib
import re

# django imports
from django.utils import simplejson as json
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import loader
from django.template import RequestContext
from django.contrib.auth.models	import User


# create logger
logger = logging.getLogger("Rest view")
logger.setLevel(logging.DEBUG)
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)


def restful_view(func):

	"""Decorates all restful view functions and provides some extra handling such as
	returning a JSON object on error and converting a dictionary response into a
	valid JSON object before returning it."""

	def wrap(request, *args, **kwargs):

		action = request.method.upper()

		log_info = "RESTful request:\n\t%s %s\n\t\tmethod: %s\n" % (action, request.path, action)

		for key, value in kwargs.items():
			if value == "":
				kwargs[key] = None
				value = None
			log_info += "\t\t%s: %s\n" % (key, value)

		logger.info(log_info)
		try:
			func_response = dict(func(request, action, *args, **kwargs))
		except ValueError, err:
			logger.exception("RESTFUL ERROR")
			func_response = {"status": 400, "content": ("Value error: %s" % err)}
		except Exception:
			logger.exception("RESTFUL ERROR")
			func_response = {"status": 400, "content": "restful error, please check logs"}

		logger.info(str(func_response))

		if "status" not in func_response:
			func_response['status'] = 200

		if ("csrfmiddlewaretoken" not in func_response) and ('csrfmiddlewaretoken' in request.REQUEST):
			func_response["csrfmiddlewaretoken"] = request.REQUEST['csrfmiddlewaretoken']

		json_response = json.dumps(func_response)
		http_response = HttpResponse(json_response, mimetype='application/json')

		http_response.status_code = func_response['status']

		http_response['Access-Control-Allow-Origin'] = "*"

		return http_response

	return wrap
	
def json_view(func):

	"""Decorates all restful view functions and provides some extra handling such as
	returning a JSON object on error and converting a dictionary response into a
	valid JSON object before returning it."""

	def wrap(request, *args, **kwargs):	
		try:
			func_response = dict(func(request, *args, **kwargs))
		except Exception, err:
			func_response = {"status": 500, "content": ("Request error: %s" % err)}

		if ("csrfmiddlewaretoken" not in func_response) and ('csrfmiddlewaretoken' in request.REQUEST):
			func_response["csrfmiddlewaretoken"] = request.REQUEST['csrfmiddlewaretoken']

		json_response = json.dumps(func_response)
		http_response = HttpResponse(json_response, mimetype='application/json')

		if 'status' in func_response :
			http_response.status_code = func_response['status']
		else:
			http_response.status_code = 200
		http_response['Access-Control-Allow-Origin'] = "*"

		return http_response

	return wrap


def put_splitter(func):

	"""Parses form-encoded values into a dict for parsing in a PUT request"""

	def wrap(request, *args, **kwargs):
		temp = request.read()
		temp = temp.split("&")
		request_body = []
		for val in temp:
			val = urllib.unquote_plus(val)
			request_body.append(val.split("="))
		return dict(func(request, request_body, *args, **kwargs))

	return wrap


class template_view(object):

	"""
	Decorator which wraps a view function and renders a specified template with the
	returned context dictionary (from the wrapped function) and returns it.
	"""

	def __init__(self, tmpl, pagename=""):
		self.tmpl = tmpl
		self.pagename = pagename

	def __call__(self, func):
		def wrap(req, *a, **kw):
			c = {
			#	"user" : User.user
			}

			d = dict(func(req, c, *a, **kw))
			
			d["legacy_browser"] = False
			if re.search("MSIE\s[5-9]", req.META["HTTP_USER_AGENT"]):
				d["legacy_browser"] = True
				
			if "message_error" in d:
				message = "ERROR: %s" % d["message_error"]
				logger.exception(message)

			if 'redirect' in d:
				return HttpResponseRedirect(d['redirect'])

			rc = RequestContext(req, d)
			t = loader.get_template(self.tmpl)
			response = HttpResponse(t.render(rc))
			
			if 'content_type' in d:
				response["Content-Type"] = d["content_type"]
			if 'status' in d:
				response.status_code = d["status"]

			return response

		return wrap


class check_planneruser_permission(object):

	"""
	Decorator to check that a PlannerUser instance has permission to
	perform a specified action

	Example:
	>>> @check_planneruser_permission("permission_adduser")
	>>> def some_view(request):
	>>>     pass

	If the user has the specified permissions, the decorated function will be
	executed and returned, if not, the following JSON object is returned:
	>>> {"status": 403, "content": "Operation forbidden" }
	"""

	def __init__(self, permission=""):
		self.permission = permission

	def __call__(self, func):
		def wrap(request, *args, **kwargs):
			try:
				user = RequestContext(request)["user"].planneruser
			except Exception:
				return {
					"status": 403,
					"content": "Operation forbidden, please log in"
				}
			# check the user to have permissions to add a task
			if getattr(user, self.permission):
				return dict(func(request, *args, **kwargs))
			return {
				"status": 403,
				"content": "Operation forbidden"
			}

		return wrap
