from django.db	         			import models
from django.contrib					import auth
from django.contrib.auth.models		import User
from django.utils 					import simplejson as json

from datetime 						import datetime
import convert

class AppModel( models.Model ):	
	created = models.DateTimeField(default=datetime.today)
	modified = models.DateTimeField(default=datetime.today)
	
	def save(self,*args,**kw):
		self.modified = datetime.now()
		return super(AppModel,self).save(*args,**kw)
		
	class Meta:
		abstract = True
		
class Article( AppModel ):
	title = models.CharField(max_length=200)
	slug = models.CharField(max_length=200)
	data = models.TextField()
	content = models.TextField()
	type = models.IntegerField()	
	
	private = models.BooleanField(default=False)
		
	user = models.ForeignKey(User)
	parent = models.ForeignKey("self",related_name="children",null=True)
	
	def todict(self,**kw_args):
		parent = None
		if self.parent : parent = self.parent.id
		
		levels = 5
		if 'levels' in kw_args:
			levels = kw_args["levels"]
		
		types = Config.get('article_types').split(",")
		
		try:
			data = dict( json.loads( self.data ) )
		except Exception, err:
			data = {}
			
		out = {
			'id' : self.id,
			'title' : self.title,
			'slug' : self.slug,
			'data' : dict( json.loads( self.data ) ),
			'content' : self.content,
			'markdown' : convert.format( self.content, 2 ),
			'type' : self.type,
			'private' : self.private,
			'created' : self.created.strftime("%d %B %Y, %H:%M"),
			'created_utc' : self.created.strftime("%Y-%m-%d"),
			'user' : {
				"id" : self.user.id,
				"username" : self.user.username,
				"firstname" : self.user.first_name,
				"lastname" : self.user.last_name,
				"is_staff" : self.user.is_staff
			},
			'comments' : [],
			'children' : [],
			'parent' : parent
		}
		#Append Children
		if levels > 0:
			children = self.children.all().order_by('-created')
			for c in children:
				out['children'].append( c.todict(levels=levels-1) )
				
		#Append Comments
		if 'comments' in kw_args or True:
			comments = Comment.objects.filter(article_id__exact=self.id).order_by('created')
			for c in comments:
				out['comments'].append( c.todict() )
				
		return out
		
	@staticmethod
	def createSlug( title ):
		#Slugify title
		import re
		title = title.lower()
		title = re.sub("[^a-z0-9_\s]","",title)
		title = title.strip()
		title = re.sub("\s+","_",title)
		
		
		#Check if slug already exists
		new = True
		count = 0
		final_title = title
		while new:
			if count > 0:
				final_title = title + str(count)
			new = Article.objects.filter(slug=final_title).count() > 0
			count += 1
			
		#return slug
		return final_title
		
	def save(self,*args,**kw):
		if self.slug == "":
			self.slug = Article.createSlug( self.title )
		return super(Article,self).save(*args,**kw)
	
class Comment( AppModel ):
	article = models.ForeignKey(Article)
	user = models.ForeignKey(User)
	content = models.TextField()
	
	def todict(self,**kw_args):
		out = {
			'id' : self.id,
			#'start' : self.title,
			#'end' : self.content,
			'content' : self.content,
			'markdown' : convert.format( self.content ),
			'article_id' : self.article.id,
			'created' : self.created.strftime("%d %B %Y, %H:%M"),
			'user' : {
				"id" : self.user.id,
				"username" : self.user.username,
				"firstname" : self.user.first_name,
				"lastname" : self.user.last_name,
				"is_staff" : self.user.is_staff
			}
		}
		return out
	
class Config( AppModel ):
	name = models.CharField(max_length=100,unique=True)
	value = models.TextField()
	
	@staticmethod
	def get( key ):
		try:
			value = Config.objects.get(name=key).value
		except Exception, err:
			value = None
		
		if value is None:
			if key == "article_types":
				value = "none,default"
			if key == "image_sizes":
				value = '{"tiny":89,"small":100,"medium":157,"large":728,"orig":-1}'
			if key == "custom_fields":
				value = "{}"
			if key == "image_old_upload":
				value = "False"
		
		#No default, just get
		return value