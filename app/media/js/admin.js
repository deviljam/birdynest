$(document).ready(function(){
	$('.image_select .btn').click(function(e){
		var image_window = window.open('/admin/images', 'images', 'width=350,height=600,scrollbars=yes');
		this.image_window = image_window;
		
		this.image_window.parent_select_field = $(this).parent().find('input')[0];
		/*this.image_window.alert(image_window.parent_select_field);*/
		
		var self = this;
		setTimeout( function(){ 
			self.image_window.onbeforeunload = function(){ 
				var value = self.image_window.document.getElementById("selected_image").value;
				$(self).parent().find('input').val( value );
			} 
		}, 200 );
		
	});
	$('.delete').on('click', function(e){
		if( confirm("Are you sure you want to delete this article?") ) {
			return true;
		} else {
			e.preventDefault();
			return false;
		}
	});
});