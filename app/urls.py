from django.conf.urls 				import patterns, include, url
from django.conf.urls.defaults 		import * 
import settings
from custom_urls import urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns += patterns('',
	url(r'^img/uploads/([^/]+)?$', 'app.blog.admin.image_request'),
	url(r'^(?P<path>(js|css|img)/.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	url(r'^(favicon\.ico)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	
	url(r'^login/?(.*)$', 'app.blog.admin.user_login'),
	url(r'^logout/?$', 'app.blog.admin.user_logout'),
	url(r'^admin/?$', 'app.blog.admin.index'),
	
	url(r'^admin/images/?$', 'app.blog.admin.image_manager'),
	url(r'^admin/upload_image?$', 'app.blog.admin.upload_image'),
	
	url(r'^admin/users/edit/(\d)+/?$', 'app.blog.admin.users_edit'),
	url(r'^admin/users/?$', 'app.blog.admin.users_index'),
	
	url(r'^admin/articles/?$', 'app.blog.admin.articles_index'),
	url(r'^admin/articles/add?$', 'app.blog.admin.articles_create'),
	url(r'^admin/articles/edit/(\d+)/?$', 'app.blog.admin.articles_edit'),
	url(r'^admin/articles/delete/(\d+)/?$', 'app.blog.admin.articles_delete'),
	
	url(r'^admin/config/?$', 'app.blog.admin.config_index'),
	url(r'^admin/config/add?$', 'app.blog.admin.config_create'),
	url(r'^admin/config/edit/(\d+)/?$', 'app.blog.admin.config_edit'),
	
	url(r'^api/users/logout/?$', 'app.blog.api.user_logout'),
	url(r'^api/users/login/?$', 'app.blog.api.user_login'),
	url(r'^api/users/register/?$', 'app.blog.api.user_register'),
)

handler404 = 'app.views.error_404'